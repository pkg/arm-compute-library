The workflow we are using for arm-compute-library is based on git-debrebase and
dgit. See dgit‐maint‐debrebase(7) for details.

Getting the source
------------------

Use dgit clone to obtain the latest version of the code:

  dgit clone arm-compute-library

The command above should result in a checkout with two remotes: vcs-git and
dgit. The former is the repository on salsa. Now add the upstream repository as
a remote:

  git remote add -f upstream https://github.com/ARM-software/ComputeLibrary

Packaging a new upstream release
--------------------------------

Get all the latest upstream tags:

  git fetch --tags upstream

In case any non-DFSG material needs to be dropped:

  git checkout -b upstream-dfsg v23.08
  git rm data/ -rf
  git commit -m "Upstream version 23.08 DFSG-cleaned"
  git tag 23.08+dfsg
  git push vcs-git upstream-dfsg
  git push vcs-git --tags

Import the dfsg-clean new upstream with:

  git debrebase new-upstream 23.08+dfsg

Generate the new upstream tarball:

  git deborig

Changes to the upstream source code
-----------------------------------

IMPORTANT: do not touch any of the files in debian/patches/. They are
automatically generated, modifying them by hand will lead to issues.  The whole
point of using dgit and this workflow is not having to deal with patch files,
quilt, and diffs of diffs.

Any changes to the upstream code can be made directly to the upstream files
themselves, and committed with git commit. To update debian/patches/ and build
the code:

 git debrebase
 dgit build # or dgit sbuild

In case you want to reorder the patches or edit them, use `git debrebase -i`.

Packaging changes
-----------------

Just change whatever needs to be changed in the debian/ directory, EXCEPT
debian/patches/. The files in there should not be modified as mentioned in the
previous section. Use git commit at will.

Publishing the changes and uploading
------------------------------------

If you have reached a reasonably consistent point and you want to share your
work with others, but you do not want to upload to the archive yet:

  git debrebase conclude
  git push vcs-git dgit/sid

When you're ready to upload:

  dgit push-source
  git push vcs-git dgit/sid
