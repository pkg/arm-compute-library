#!/usr/bin/make -f
# See debhelper(7) (uncomment to enable)
# output every command that modifies files on the build system.
DH_VERBOSE = 1

# see EXAMPLES in dpkg-buildflags(1) and read /usr/share/dpkg/*
DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/default.mk

%:
	dh $@ --buildsystem=none

ifeq ($(DEB_TARGET_ARCH),armhf)
ACL_ARCH_OPTS := arch=armv7a neon=1
else ifeq ($(DEB_TARGET_ARCH),arm64)
ACL_ARCH_OPTS := arch=arm64-v8a neon=1
else ifeq ($(DEB_TARGET_ARCH),amd64)
ACL_ARCH_OPTS := arch=x86_64 neon=0
endif

VERSION=$(shell dpkg-parsechangelog -S version | awk '{split($$0,a,"-"); print a[1]}')

SCONS=scons
OPTIONS := $(ACL_ARCH_OPTS) opencl=1 asserts=0 Werror=0 os=linux build=native embed_kernels=1 set_soname=1 validation_tests=0 benchmark_tests=0
# Build log should be verbose unless 'terse' is set
ifneq (,$(filter terse,$(DEB_BUILD_OPTIONS)))
    OPTIONS += -s
endif

ifneq (,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
    NUMJOBS = $(patsubst parallel=%,%,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
    OPTIONS += -j$(NUMJOBS)
endif
# support nostrip too?

ifeq (,$(filter debug,$(DEB_BUILD_OPTIONS)))
    OPTIONS += debug=0
else
    OPTIONS += debug=1
endif

override_dh_auto_build-arch:
	$(SCONS) $(OPTIONS) install_dir=install
	# Generate pkgconfig files
	for lib in arm_compute arm_compute_graph; do \
	sed -e "s/@DEB_HOST_MULTIARCH@/$(DEB_HOST_MULTIARCH)/" < debian/lib$${lib}.pc.in > debian/$${lib}.pc && \
	sed -i -e "s:@VERSION@:$(VERSION):" debian/$${lib}.pc; \
	done

override_dh_auto_build-indep:
	mkdir -p build/docs
	doxygen docs/Doxyfile && touch debian/docs.stamp

DOCDIR=debian/tmp
override_dh_auto_install-indep: debian/docs.stamp
	mkdir -p $(DOCDIR)
	cp -a --reflink=auto build/docs/html $(DOCDIR)
	cp -a debian/ACL_logo.png $(DOCDIR)

override_dh_auto_clean:
	dh_auto_clean
	rm -rf build
	rm -rf documentation
	rm -rf .sconf_tem/ .sconsign.dblite config.log
	rm -f debian/arm_*.pc
	rm -f debian/docs.stamp
